This project borrows heavily from the excellent [Laravel 4](http://laravel.com) and [Symfony 2](http://symfony.com) projects.

## License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

## Standalone
~~~~
<?php
include 'vendors/autoload.php';
$loader = new \Smorken\View\FileLoader(__DIR__ . '/views'); represents base path where view files are stored
$view = new \Smorken\View\PhpView($loader, 'master');
~~~~

## Part of simple app

Add a service line to config/app.php services array

```
'Smorken\View\ViewService'
```

Add a view.php file to the config directory

~~~~
return array(
    'provider' => 'Smorken\View\PhpView',
    'loaderopts' => array(
        \Smorken\Utils\PathUtils::base() . '/views',
    ),
    'master' => 'master',
);
~~~~