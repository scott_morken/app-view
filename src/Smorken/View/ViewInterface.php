<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 11:46 AM
 */

namespace Smorken\View;


interface ViewInterface {

    /**
     * Renders (or returns) the result of the request $view
     * @param string $view name of view to render
     * @param array $data injected variables
     * @param bool $output render or return view
     * @return mixed
     */
    public function render($view, $data, $output = true);

    /**
     * Partial rendering of a view
     * Doesn't render master
     * @param string $view
     * @param array $data
     * @param bool $output
     * @return null|array
     */
    public function renderPartial($view, $data = array(), $output = true);

    /**
     * Adds shareable view data as an array of keys => values
     * @param array $data
     * @return mixed
     */
    public function addShares(array $data);

    /**
     * Adds a single shareable view data
     * @param $key
     * @param $value
     * @return mixed
     */
    public function addShare($key, $value);

} 