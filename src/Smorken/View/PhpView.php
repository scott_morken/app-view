<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 11:46 AM
 */

namespace Smorken\View;

/**
 * Class PhpView
 * @package Smorken\View
 *
 * Very simple view handler
 * Uses regular php with html
 * <html><?php echo $mycontent; ?></html>
 */
class PhpView implements ViewInterface {

    /**
     * @var LoaderInterface
     */
    protected $loader;

    /**
     * Master view name
     * @var null|string
     */
    protected $master;

    /**
     * additional data shared with the view
     * @var array
     */
    protected $add_data = array();

    public function __construct(LoaderInterface $loader, $master = null)
    {
        $this->loader = $loader;
        $this->master = $master;
    }

    /**
     * Set the master view
     * @param $master
     */
    public function setMaster($master)
    {
        $this->master = $master;
    }

    /**
     * Add shareable data to the view
     * @param array $data
     */
    public function addShares(array $data)
    {
        $this->add_data = $data;
    }

    /**
     * Add shareable data by key to the view
     * @param $key
     * @param $value
     */
    public function addShare($key, $value)
    {
        $this->add_data[$key] = $value;
    }

    /**
     * Renders the file specified by $view with any variables injected from $data
     * if $output is true, the result is echoed, otherwise it is returned
     * if a master view exists, the result from this view is inserted into the master
     * view in the $content variable, the other injected vars are shared with master as well
     * @param string $view
     * @param array $data
     * @param bool $output
     * @return null|string
     */
    public function render($view, $data = array(), $output = true)
    {
        list($content, $ndata) = $this->renderPartial($view, $data, false);
        if ($this->master) {
            list($html, ) = $this->renderPartial($this->master, array_merge($ndata, $data, array('content' => $content)), false);
        }
        else {
            $html = $content;
        }
        if ($output) {
            echo $html;
        }
        else {
            return $html;
        }
    }

    /**
     * Partial rendering of a view
     * Doesn't render master
     * @param string $view
     * @param array $data
     * @param bool $output
     * @return null|array
     */
    public function renderPartial($view, $data = array(), $output = true)
    {
        $data = array_merge($this->add_data, $data);
        list($content, $ndata) = $this->loader->load($view, $data);
        if ($output) {
            echo $content;
        }
        else {
            return array($content, $ndata);
        }
    }

}