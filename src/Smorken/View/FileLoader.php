<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 11:46 AM
 */

namespace Smorken\View;


class FileLoader implements LoaderInterface {

    protected $basepath;

    /**
     * Instantiate the file loader with the base path to the view files
     * @param string $basepath
     */
    public function __construct($basepath)
    {
        $this->basepath = $basepath;
    }

    /**
     * Returns an array of the html from the view file and the currently
     * available variables
     * @param string $view
     * @param array $data
     * @return array($html, $data)
     * @throws LoaderException if view cannot be loaded
     */
    public function load($view, $data = array())
    {
        $file = "{$this->basepath}/{$view}.php";
        if (!file_exists($file)) {
            throw new LoaderException("$view could not be loaded.");
        }
        ob_start();
        extract($data);
        require $file;
        $defined = get_defined_vars();
        $skip = array('GLOBALS', '_POST', '_GET', '_COOKIE', '_FILES', '_SERVER', '_SESSION',
            'argv', 'data', 'view', 'file');
        foreach($defined as $k => $v) {
            if (!in_array($k, $skip) && !array_key_exists($k, $data)) {
                $data[$k] = $v;
            }
        }
        $html = ltrim(ob_get_clean());
        return array($html, $data);
    }

} 