<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 12:00 PM
 */

namespace Smorken\View;


use Smorken\Service\Service;

class ViewService extends Service {

    protected $deferred = true;

    public function start()
    {
        $this->name = 'view';
    }

    /**
     * Registers 'view' to App
     * In this case, we look up a provider from config/view - provider,
     * any options from config/view - loaderopts,
     * create a new FileLoader (for handling the looking up/loading of the
     * template files),
     * and return an instance of provider with the loader and the master view (if set)
     */
    public function load()
    {
        $this->app->instance($this->name, function($c) {
            $provider = $c['config']['view.provider'];
            $loaderopts = $c['config']['view.loaderopts'];
            $ref = new \ReflectionClass('Smorken\View\FileLoader');
            $loader = $ref->newInstanceArgs($loaderopts);
            return new $provider($loader, $c['config']['view.master']);
        });
    }
} 