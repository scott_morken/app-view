<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 7/24/14
 * Time: 11:46 AM
 */

namespace Smorken\View;


interface LoaderInterface {

    /**
     * Loads a view from a file/whatever the backend provider is
     * @param string $view name of view to load
     * @return array(string $content, array $data)
     */
    public function load($view);

} 